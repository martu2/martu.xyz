---
layout: post
title:  "Guía rápida para activistas"
date:   2020-02-05 17:06:36 +0000
categories: proteccion_digital
---


# Guía rápida para activistas: como transitar de google a otros servicios más seguros y libres

- gmail --> [https://riseup.net/es/email](https://riseup.net/es/email) u [otros](/activistas/2020/02/05/correos-seguros.html).
- google groups --> [https://riseup.net/es/lists](https://riseup.net/es/lists)
- google docs (con info publica)--> [https://pad.riseup.net](https://pad.riseup.net) u [otros](/activistas/2020/02/05/pads.html)
- google docs (con info sensible) --> [https://cryptpad.fr/](https://cryptpad.fr/)
- google forms --> [https://framaforms.org/](https://framaforms.org/)

**Otros servicios interesantes que quizás tu colectiva necesita:**

- Mensajería instantánea (tipo whatsapp): [https://www.signal.org/](https://www.signal.org/)
- Video-llamadas de grupo: [https://meet.greenhost.net/](https://meet.greenhost.net/)
- Blogs: [https://noblogs.org/](https://noblogs.org/)
- Encuestas (tipo doodle): [https://framadate.org/](https://framadate.org/) o [https://poll.disroot.org/](https://poll.disroot.org/)
- Calendario: [https://framagenda.org/](https://framagenda.org/) o [https://tutanota.com/es/calendar/](https://tutanota.com/es/calendar/)


Y RECUERDA: ESTOS SERVICIOS NO SON TAN BONITOS (AUNQUE ALGUNOS SÍ!) COMO LOS DE GOOGLE, PERO TRABAJAN PARA QUE ACTIVISTAS COMO TÚ PUEDAN SEGUIR LUCHANDO! TEN PACIENCIA!

Si tu colectiva puede ¡DONA!
