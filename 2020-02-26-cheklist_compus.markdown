---
layout: post
title:  "Cuidados básicos para computadoras"
date:   2020-02-26 18:06:36 +0000
categories: hackfeminismo proteccion_digital
---
# Cheklist de cuidados básicos para computadoras

&#9989; Tengo pegatinita en la cámara.

&#9989; Tengo una contraseña para el inicio de sesión (¡fuerte!). ([Sigue aquí](https://securityinabox.org/en/guide/basic-security/windows/#how-to-password-protect-a-windows-user-account))

&#9989; Mi equipo se suspende a los pocos minutos de no usarlo. ([Sigue aquí](https://securityinabox.org/en/guide/basic-security/windows/#screen-lock))

&#9989; ¡Mi sistema operativo está actualizado! ([Sigue aquí](https://securityinabox.org/en/guide/basic-security/windows/#system-updates-windows-7---8.1))

&#9989; Mis programas instalados están actualizados (en su última versión)

&#9989; Mi antivirus está actualizado y la base de datos de virus también. Si uso Windows 8 o 10 puedo usar Windows Defender (ya viene instalado).  

&#9989; No tengo varios antivirus instalados a la vez.

&#9989; Reviso periódicamente mi equipo con un antimalware. (Por ejemplo: malwarebytes - uso la versión de prueba y luego desinstalo)

&#9989; Tengo el firewall activado ([Sigue aquí](https://securityinabox.org/en/guide/basic-security/windows/#windows-firewall))

&#9989; No instalo programas de webs desconocidas o no oficiales. ¡NUNCA DE SOFTONIC!

&#9989; Tengo habilitada la opción de ver la extensión de los archivos y de ver los archivos ocultos.

&#9989; No ejecuto archivos con la extensión .exe u otras que no reconozco o que no sé que que son, podrían ser virus.

&#9989; Paso el antivirus por los pendrives que han estado en otros equipos ajenos. Los formateo con frecuencia.

&#9989; Hago copias de seguridad periódicamente, que guardo de forma segura en otro disco duro/dispositivo.

&#9989; Tengo mi información ordenada y elimino los archivos que no necesito, vacío la papelera con frecuencia.

&#9989; Si creo que mi equipo no funciona correctamente me intereso en detectar porqué, converso con compañeras con más conocimientos técnicos. Quizás sea necesario formatear el equipo, renovar/ampliar componentes o incluso sustituirlo.

&#9989; Los recipientes con líquidos cercanos a mi equipo siempre tienen tapa (botellas con tapón, tazas con tapa, termos cerrados)

&#9989; No tengo post-its con contraseñas apuntadas en mi escritorio. Tampoco en papeles o libretas.

&#9989; Uso un gestor de contraseñas, como KeePassXC

 *Esta es una lista en construcción, escríbeme si crees que sería bueno añadir más cositas*

Puedes descargar en formato pdf [aquí](/media/cheklist_seg_basica_equipos.pdf).

<br>

<center><i>Trato de mantener ese equilibrio entre cuidados y  "productividad".</i></center>

![ciberseguras](/media/somos.png)
<center>(imagen de ciberseguras.org)</center>
