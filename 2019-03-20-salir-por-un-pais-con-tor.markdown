---
layout: post
title:  "Como salir a internet por un nodo de Tor de un país concreto"
date:   2019-03-20 18:06:36 +0000
categories: Tor Linux
---

Pues ayer aprendí a usar un nodo de Tor concreto para salir a internet! No es un misterio muy grande!

**Solo hay que editar un fichero, añadir una linea y reiniciar Tor!**

El fichero a editar es `torrc` que en mi caso estaba en:

`/home/XXXX/Descargas/tor-browser-linux64-8.0.5_en-US/tor-browser_en-US/Browser/TorBrowser/Data/Tor`

Y la línea a añadir es la siguiente: 

`ExitNodes {ar} StrictNodes 1`

Si pones esta línea te saldrá por Argentina, pero puedes poner el código de otros país, en esta web puedes encontrar los códigos por países: 

[https://b3rn3d.herokuapp.com/blog/2014/03/05/tor-country-codes/](https://b3rn3d.herokuapp.com/blog/2014/03/05/tor-country-codes/) 

Y aquí la web donde puedes ver que nodos de tor están disponibles por países:

[https://metrics.torproject.org/rs.html#](https://metrics.torproject.org/rs.html#)

Para hacer una búsqueda por país la query a hacer sería: 

`country:ar type:relay running:true`

Espero que les sirva! <3

![](https://tor.derechosdigitales.org/torificate/tortola1-2.png) 

[Imágen de Derechos Digitales cc-by-sa 2018](https://tor.derechosdigitales.org/torificate/) 
