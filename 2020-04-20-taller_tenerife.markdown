---
layout: post
title:  "Taller protección protección digital para activistas"
date:   2020-04-20 18:06:36 +0000
categories: proteccion_digital
---

**¡Ven al taller de protección digital para activistas!**

Será el **viernes 24 de abril, de 18 a 20h (hora canaria)*** en esta sala de Jitsi: https://vc.autistici.org/taller_proteccion_digital

Para conectarte tienes que abrir ese enlace en el navegador Chrome o Chromium desde el ordenador. Si te quieres conectar desde el móvil tienes que abrirlo desde la app [Jitsi Meet](https://link.siick.fr/s).

Si tienes dudas para conectarte puedes escribir a m4rtu[arroba]riseup.net.

Este será un **taller introductorio** orientado a **activistas de Tenerife (y el exterior)** con pocos conocimientos informáticos que necesitan **herramientas seguras para organizarse**. Trataremos de que sea un **espacio seguro para todxs**, especialmente para mujeres y personas LGTBIQ que no suelen tener presencia en espacios _tech_.

¡Apúntate! **¡No habrán preguntas tontas!** <3

*La hora de las Islas Canarias es Western European Summer Time UTC+1 - La misma que Londres.

![imagen](/media/cartel2.jpg)
