---
layout: post
title:  "¿Qué es información sensible?"
date:   2020-02-26 18:06:36 +0000
categories: hackfeminismo proteccion_digital
---

# Ejercicio para identificar que es "información sensible"

1. Lista todos los dispositivos/lugares donde almacenas información, incluyendo:

* Algunos dispositivos podrían ser:
  - Ordenadores/computadoras (personal, de trabajo, compartido)
  - Móviles/celulares (personal, de trabajo)
  - Tablets (personal, de trabajo, compartido)
  - Discos duros externos
  - Pendrives o llaves USB
  - Cámaras de vídeo y fotos
  - Tarjetas SD y microSD

* Otros lugares podrían ser:
  - Servidores de almacenamiento online - nubes (GoogleDrive, Dropbox, OneDrive, NextCloud, etc).
  - Servidores de correo electrónicos (Servidores de Gmail, Hotmail, Yahoo, RiseUp, etc)- ¿Tus correos quedan almacenados en el servidor de correo de la empresa/organización que te da el servicio de correo?.
  - Servidores de mensajería instantánea (Servidores de whatsapp, telegram, Signal, Wire, etc) - ¿Tus mensajes y archivos intercambiados por mensajería instantánea quedan almacenados en sus servidores?
  - Servidores de almacenamiento en red local (NAS) - en algunas organizaciones, empresas o comunidades se usan estos servidores.
  - Servidor de alojamiento web - por ejemplo si tienes una web con formularios y ahí recibes mensajes.

2. Haz una tabla con todos estos lugares, por ejemplo así:

   | Mi compu personal | Compu del trabajo | Movil | Disco duro externo | Nube | Correo |
   | ----------------- | ----------------- | ----- | -------------------|----- | ------ |
   |   |   |   |   |   |   |  
   |   |   |   |   |   |   |


3. Escribe debajo de cada cabecera la info que tienes en cada dispositivo/lugar, intenta incluir todos los detalles que puedas.

| Mi compu personal | Compu del trabajo | Movil | Disco duro externo | Nube | Correo |
| ----------------- | ----------------- | ----- | -------------------|----- | ------ |
| Mis CVs  | Informes sobre la situación de los DD.HH en mi región  | Toda mi agenda de contactos  | Porno lésbico  |   |   |
| Fanzines  | Investigación osint sobre grupos de ultraderecha   | Fotos con contenido sexual | Fotos de viajes   |   |   |  
| Documentales Activistas   | Datos personales e historial de usuarias del servicio de atención   | Carteles de convocatorias   |   |   |   |


4. Trata de ordenar la información de más o menos sensible, según tu propia valoración. Sitúa la información más sensible más arriba en la tabla y la menos sensible más abajo. Puedes añadir una nueva columna para evaluar el grado de sensibilidad. O también puedes usar colores.

| Sensibilidad | Mi compu personal | Compu del trabajo | Movil | Disco duro externo | Nube | Correo |
| ----------------- | ----------------- | ----- | -------------------|----- | ------ |------ |
| +++ | Mis CVs  | Datos personales e historial de usuarias del servicio de atención   | Fotos mias con contenido sexual  | Porno lésbico   |   |   |  
| |Documentales Activistas   | Investigación osint sobre grupos de ultraderecha   | Toda mi agenda de contactos | Fotos de viajes  |   |   |   
| |   |  Informes sobre la situación de los DD.HH en mi región | Carteles de convocatorias  | |   |   |
| |   |   |    |   |   |   |
| - - - | Fanzines   |   |   |   |   |   |  

5. Algunas preguntas que puedes hacerte para evaluar el grado de sensibilidad de la información que almacenas:
  - ¿Contiene **información personal identificable (IPI)** de ti o de otras personas?
      La IPI es información que identifica a una persona de forma única, por ejemplo:
        * el número de DNI, NIE o Pasaporte
        * el nombre legal completo
        * una foto de la cara
        * la matricula del coche
        * el número de teléfono (cuando está relacionado con un pasaporte/ID)
        * datos biometricos (huella dactilar, iris, etc)
        * numero de la seguridad socia
  - ¿Contiene **información personal sensible** tuya o de otras personas?
    La IPS es información con la que potencialmente se podría discriminar, criminalizar o vulnerar la privacidad de una persona, por ejemplo:    
        * el historial médico
        * el historial de antecedentes personales
        * identidad sexual / orientación sexuales

  --> Si la info que estás evaluando contiene IPI o IPS mía o de otras personas: entonces la info es sensible.

6. Otras preguntas que puedes hacerte:
  - ¿Qué pasaría si actorxs interesadxs accedieran a la información?
  - ¿Qué tan grabe sería para tu organización? ¿para ti?

    * Si sería muy grabe: la info es muy sensible
    * Si supusiera un costo importante pero superable: la info es sensible
    * Si el impacto sería mínimo: la info no sensible
