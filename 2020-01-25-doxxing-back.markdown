---
layout: post
title:  "Doxxing back"
date:   2020-01-25 18:06:36 +0000
categories: Hackfeminismo Hackback
---

## ¿Que es eso del doxxing?

*Doxxing*, es eso de buscarnos en internet a conciencia, recolectar nuestra **información personal identificable** (como el nombre legal, la dirección de casa y/o de trabajo, el teléfono, el mail, la matricula del coche, etc) y nuestra **información personal sensible** (como nuestra orientación sexual, historial médico, antecedentes penales, creencias religiosas, pertenencia a colectivos o cualquier otra información con la que se nos pueda discriminar o criminalizar) que pueda estar por algún lugar de la red, para luego publicarla en internet toda junta con la intención de dañarnos, de que se nos encuentre, se nos localice, de que otras personas nos acosen por mail o por teléfono, etc. Esta es una forma de violencia en internet que suelen enfrentar las activistas, las defensoras de derechos humanos y las mujeres y personas LGTBIQ en general.

Puede venir unida de *outing* (que alguien difunda tu orientacion/identidad sexual sin consentimiento), porno venganza o difusión de imágenes sexuales sin consentimiento, etc.

Doxxing viene de "dox" que es una abreviatura de "docs" (documentos en ingles), [acá](https://es.wikipedia.org/wiki/Doxing) una definición de la wikipedia.

Acá propongo hablar de la estrategia de hacer doxxing como forma de autodefensa digital, es decir hacer doxxing a los agresores, hacer doxxing a los que hacen doxxing, o sea:

DOXXING BACK ;-)

Además al aprender las herramientas que se usan para hacer *doxxing* puedes aprender a prevenirlo contra ti o tus colectivas, que también está muy bien.

Vamos a ellos, por partes, de forma sencilla:

## 1. Servicios Whois

Todos los dominios (esa dirección o nombre que tiene una página web, ejemplo: foromotos.es) se registran a través a empresas o entidades gestoras de dominios.

Cuando una persona o entidad compra (o más bien, registra) un dominio obligatoriamente tienen que dar unos datos que la identifican (nombre legal, email, telefono, dirección) y se supone que estos datos tienen que ser reales. Si estos datos son incorrectos, no son reales o no están actualizados te pueden suspender el dominio, así que la mayoría de personas/entidades da datos reales.

Existe un protocolo que se llama WHOIS que sirve para conocer los datos de la persona o entidad que registró un dominio.

Aquí puedes leer más acerca de este protocolo: https://whois.icann.org/es/acerca-de-whois

Si solo quieres probarlo puedes usar servicios online para consultar los datos de la persona registradora de un dominio. Aquí tienes algunos servicios:

* El propio de la ICANN: https://lookup.icann.org/
* El de GoDaddy: https://es.godaddy.com/whois
* https://who.is/
* Muchas empresas gestoras de dominios tienen su propio servicio de whois.

Si usas GNU/linux puedes usar el comando `whois eldominioquequieras.com`

Puede ser que de ciertos dominios no encuentres información porque se haya configurado el whois privado ([who privacy](https://en.wikipedia.org/wiki/Domain_privacy)), cosas que cada vez más personas hacen.

Si tu tienes una página web también lo puedes hacer para evitar que te doxxen a ti. Pregunta a la entidad/empresa con la que registraste el dominio.

## 2. Búsquedas más allá de google

Más allá de buscar directamente en google puedes probar a usar otros buscadores alternativos, que además de no añadir filtros de burbujas (algoritmos que adaptan resultados según tus búsquedas anteriores, tu localización, etc), que podrían evitar información relevante para tu investigación, respetan tu privacidad y no comercian con tus datos.

Algunos son:

* https://www.startpage.com/
* https://duckduckgo.com/

También puedes explorar la búsqueda avanzada de google https://www.google.es/advanced_search que te da muchas más opciones para hacer una búsqueda más restringida.

## 3. Busca su nickname en cientos de RR.SS

A veces sabemos el nickname o username que usa un agresor por que nos ha escrito a través de una red social.

Muchas veces no son muy listos y utilizan el mismo nick/nombre/user en diferentes redes sociales o foros.

Podemos buscarlo con https://namechk.com/ que es un servicio que chequea si un nickname ya está usado en miles de redes sociales (que ni sabias que existían jeje)

Quizás descubrimos que usa una red social donde da algo de información sobre él o alguna otra pista...

## 4. Búsquedas por imágenes

También podemos tener una imagen que identifica al agresor, su avatar o otras imágenes que nos puedan ayudar llegar a él.

Existen buscadores de imágenes que en vez de usar palabras para buscar puedes subir una imagen. Los resultados te darán los sitios webs donde se encuentre esa imagen o imágenes parecidas.

* Está el propio buscador de imágenes de google: https://images.google.com/
* y una alternativa es https://tineye.com/

## 5. Revisa las cabeceras del email

Si recibes un email del agresor puedes tratar de averiguar desde que ip te escribe, muchos correos electrónicos ocultan la ip de origen pero muchos otros aún no lo hacen, no pierdes nada por revisar la "cabeceras del email" por si de ahí puedes sacar algo de información.

Al menos podrás saber la información del proveedor servicio de email que usa la persona que quizás te da alguna pista.

La cabecera de un correo se puede ver normalmente en alguna pestaña que pone "ver cabecera" o "ver código fuente" o algo así. Ahí deberías las direcciones IP de los servidores por donde ha pasado el correo y en ocasiones la IP del equipo de salida.

Aquí el maldito google te explica como llegar a las cabeceras de gmail y de otros servicios de mail: https://support.google.com/mail/answer/29436?hl=es

Aquí un servicio (hay muchos mas online) donde puedes chekear donde está ubicada una ip https://www.cual-es-mi-ip.net/geolocalizar-ip-mapa

Recuerda que quizás la persona que te mando el mail o el proveedor de mail que está usando además utiliza una VPN para ocultar su IP real así que puede ser que la IP que veas en las cabeceras no sea la real.

También puedes pensar tu en ocultar tu IP cuando mandas correos, para que no te doxxen a ti, puedes usar RiseupVPN https://riseup.net/en/vpn o https://psiphon.ca/es/download.html

## 6. Busca en los metadatos

Cualquier archivo tiene unos metadatos, que hablan de las propiedades del archivo, cuando se creo, quien es le autore y algunas cositas más. En el caso de las imágenes y vídeos, los metadatos vienen en un formato que se llaman Exif, los datos Exif o metadatos de una imagen/vídeo puede traer información detallada de con qué cámara se tomó, si fue con un móvil dirá el modelo, la fecha, la hora y hasta la localización en coordenadas GPS (si el dispositivo que las tomó tenía un GPS activado y configurado para que se guardara en los metadatos por defecto).

Tienes que saber que los metadatos se puede editar y borrar (¡menos mal!) pero también es verdad que a veces se nos pasa limpiar la foto/vídeo de metadatos antes de subirla o enviarla, a nuestro agresor también le puede pasar...

Aquí tines algunos servicios en línea donde puedes ver los metadatos de una imagen o vídeo:

* https://www.metadato.org/ (Si tiene datos GPS te lo muestra en el mapa)
* https://exif-viewer.com/
* https://camerasummary.com

No uses estos servicios para subir tus fotos, para ver los metadatos de tus fotos (en tu ordenador) puedes usar GIMP o otros editores de fotos. Para Android puedes usar la App [Camera Roll](https://play.google.com/store/apps/details?id=us.koller.cameraroll).

Para borrar o editar metadatos también puedes usar Camera Roll, GIMP o [Scrambled Exif](https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif&hl=es_419).

*(post en construcción)*
