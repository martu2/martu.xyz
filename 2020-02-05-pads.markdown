---
layout: post
title:  "Servicios de pads"
date:   2020-02-05 17:06:36 +0000
categories: proteccion_digital
---

# Servicios de pads

Un pad es un documento colaborativo en línea. Un servicio de pads permite crear y usar estos pads. Muchos de estos servicios usan el software de código abierto [Etherpad](https://etherpad.org/).

Pads sin contraseña en servidores feministas/activistas:
- [https://pad.kefir.red/](https://pad.kefir.red/)
- [https://pad.riseup.net/](https://pad.riseup.net/)
- [https://antonieta.vedetas.org/](https://antonieta.vedetas.org/)
- [https://pad.gaia.red/](https://pad.gaia.red/)

Pads con contraseña (tienes que registrarte y añadirle contraseña en las propiedades): [https://cryptpad.fr/](https://cryptpad.fr/)
