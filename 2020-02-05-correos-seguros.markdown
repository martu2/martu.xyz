---
layout: post
title:  "Algunos servicios de correo seguro"
date:   2020-02-05 17:06:36 +0000
categories: proteccion_digital
---

# Algunos servicios de correo seguro

### Proyectos activistas:
- Riseup: [https://riseup.net/es](https://riseup.net/es) - Requiere una invitación (puedes pedírmela a mi m4rtu[arroba]riseup.net) o rellenar un formulario y en unos días te contestan.
- Disroot: [https://user.disroot.org/](https://user.disroot.org/)
- Autistici: [https://www.autistici.org/](https://www.autistici.org/)

(recuerda donar si usas alguno de estos servicios)

### Empresas:
- Tutanota: [https://tutanota.com/es/](https://tutanota.com/es/)
- ProtonMail: [https://protonmail.com/](https://protonmail.com/)
