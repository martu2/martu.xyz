---
layout: post
title:  "En serio, ¿cómo creo contraseñas realmente seguras que pueda recordar?"
date:   2019-05-21
categories: passwords
---

Las típicas recomendaciones para una contraseña segura son:

* que sea larga: al menos 8 caracteres (aunque ya caso todo el mundo recomienda 16)
* que tenga números
* que tenga mayúsculas y minúsculas
* que tenga caracteres especiales (¿?!#&...)
* que no tenga palabras del diccionario
* que no tenga fechas
* que no tenga información personal
* que no tenga información del servicio para el que estas creando la contraseña

Fuente: [https://en.wikipedia.org/wiki/Password_policy](https://en.wikipedia.org/wiki/Password_policy)

Entonces llega el momento de hacerse una cuenta y está bastante difícil ponerse creativa con todas estas limitaciones, incluso para las personas que estamos concienciadas con la importancia de una buena contraseña.

Si se trata de contraseñas que no tienes que recordar, pues lo mejor es usar un gestor de contraseñas como keepassxc o keepassx que te las puede generar y almacenar, pero ¿Qué pasa cuando es una contraseña que si tienes que recordar? Como la del correo, la de cifrado del disco de la computadora o la del propio gestor de contraseñas

En [Security in a box](https://securityinabox.org/es/guide/passwords/) se recomienda usar "frases de contraseña", es decir, usar una frase en vez de una palabra para una contraseña. Nos muestran esta reveladora tabla:


Ejemplo de contraseña	| Número de combinaciones | 	Tiempo para descifrar
----------------------|-------------------------|-
Morada                |   	9,000	     |Inmediato
AlfombraMorada |	79 millones |	Menos de un día
AlfombraMoradaSalto	| 699 mil millones |	Menos de un día
AlfombraMoradaSaltoGaraje |	6,000 trillones |	Cuatro días
AlfombraMoradaSaltoGarajeTinte |	55 millones de trillones |	Casi un siglo
AlfombraMoradaSaltoGarajeTinteR4ro |	488 billones de trillones |	7,695 siglos

Estas tablas se basaron en calculos de http://www.passfault.com/

Fuente [https://securityinabox.org/es/guide/passwords/](https://securityinabox.org/es/guide/passwords/)

Como se puede ver contraseñas con 5 palabras del diccionario pueden muy fuertes. Parece que hay una corriente que propone usar este tipo de contraseñas en vez de contraseñas con números, caracteres especiales y otras cosas difíciles de recordar pero cortas.

Se propone usar un "diceware" para elegir palabras aleatorias, que consiste en usar 5 dados para escoger palabras de una lista. Aquí explican un ejemplo de como hacerlo [https://www.eff.org/dice](https://www.eff.org/dice).

Me pregunto si no de podría usar un libro abriendo páginas al azar u otro tipo de documentos con texto.

Como conclusión, parece que usar frases como contraseñas que contengan mas de 5 palabras aleatorias (siendo las palabras de mas de 4 caracteres) es seguro.

Comprobé la contraseña "AlfombraMoradaSaltoGarajeTinte" en [http://www.passfault.com/](http://www.passfault.com/) y en [keepassxc](https://keepassxc.org/), que tiene un calculador de entropía, y parece que es excelente!
